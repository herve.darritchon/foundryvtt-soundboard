import {SBPackageManager} from './helpers/soundboardpackagemanager.js'
import {SoundBoard} from "./helpers/soundboard.js";
import SBSocketHelper from "./helpers/soundboardsockethelper.js";
import SBAudioHelper from "./helpers/soundboardaudiohelper.js";

Hooks.once("ready", async function () {

    console.log("BHK-SB || Initializing The BHK Sound Board Module.");

    SoundBoard.packageManager = new SBPackageManager();
    Hooks.callAll('SBPackageManagerReady');
    game.settings.register('SoundBoard', 'soundboardDirectory', {
        name: game.i18n.localize('SOUNDBOARD.settings.directory.name'),
        hint: game.i18n.localize('SOUNDBOARD.settings.directory.hint'),
        scope: 'world',
        config: true,
        default: 'modules/SoundBoard/exampleAudio',
        type: String,
        filePicker: 'folder',
        onChange: value => {
            if (value.length <= 0) {
                game.settings.set('SoundBoard', 'soundboardDirectory', 'modules/SoundBoard/exampleAudio');
            }
            SoundBoard.getSounds(true);
        }
    });

    // noinspection JSUnusedLocalSymbols
    game.settings.register('SoundBoard', 'source', {
        name: game.i18n.localize('SOUNDBOARD.settings.source.name'),
        hint: game.i18n.localize('SOUNDBOARD.settings.source.hint'),
        scope: 'world',
        config: true,
        type: String,
        choices: {
            'data': game.i18n.localize('SOUNDBOARD.settings.source.data'),
            'forgevtt': game.i18n.localize('SOUNDBOARD.settings.source.forgevtt'),
            's3': game.i18n.localize('SOUNDBOARD.settings.source.s3')
        },
        default: 'data',
        // eslint-disable-next-line no-unused-vars
        onChange: value => {
            SoundBoard.getSounds(true);
        }
    });
    game.settings.register('SoundBoard', 'opacity', {
        name: game.i18n.localize('SOUNDBOARD.settings.opacity.name'),
        hint: game.i18n.localize('SOUNDBOARD.settings.opacity.hint'),
        scope: 'world',
        config: true,
        type: Number,
        range: {
            min: 0.1,
            max: 1.0,
            step: 0.05
        },
        default: 0.75,
        onChange: value => {
            $('#soundboard-app').css('opacity', value);
        }
    });

    game.settings.register('SoundBoard', 'detuneAmount', {
        name: game.i18n.localize('SOUNDBOARD.settings.detune.name'),
        hint: game.i18n.localize('SOUNDBOARD.settings.detune.hint'),
        scope: 'world',
        config: true,
        type: Number,
        range: {
            min: 0,
            max: 100,
            step: 1
        },
        default: 0
    });

    game.settings.register('SoundBoard', 'allowPlayersMacroRequest', {
        name: game.i18n.localize('SOUNDBOARD.settings.macroRequest.name'),
        hint: game.i18n.localize('SOUNDBOARD.settings.macroRequest.hint'),
        scope: 'world',
        config: true,
        type: Boolean,
        default: true
    });

    // noinspection JSUnusedLocalSymbols
    game.settings.register('SoundBoard', 'forcePopoutCompat', {
        name: game.i18n.localize('SOUNDBOARD.settings.popoutCompat.name'),
        hint: game.i18n.localize('SOUNDBOARD.settings.popoutCompat.hint'),
        scope: 'world',
        config: true,
        type: Boolean,
        default: false,
        // eslint-disable-next-line no-unused-vars
        onChange: value => {
            window.location.reload();
        }
    });

    game.settings.register('SoundBoard', 'soundboardServerVolume', {
        name: 'Server Volume',
        scope: 'world',
        config: false,
        type: Number,
        default: 100
    });

    game.settings.register('SoundBoard', 'soundboardIndividualSoundVolumes', {
        name: 'Server Volume',
        scope: 'world',
        config: false,
        type: Object,
        default: {}
    });

    game.settings.register('SoundBoard', 'favoritedSounds', {
        name: 'Favorited Sounds',
        scope: 'world',
        config: false,
        default: []
    });

    // Check if an onChange fn already exists
    if (!game.settings.settings.get('core.globalInterfaceVolume').onChange) {
        // No onChange fn, just use ours
        // eslint-disable-next-line no-unused-vars
        game.settings.settings.get('core.globalInterfaceVolume').onChange = (_volume) => {
            SoundBoard.audioHelper.onVolumeChange(game.settings.get('SoundBoard', 'soundboardServerVolume') / 100);
        };
    } else {
        // onChange fn exists, call the original inside our own
        var originalGIOnChange = game.settings.settings.get('core.globalInterfaceVolume').onChange;
        game.settings.settings.get('core.globalInterfaceVolume').onChange = (volume) => {
            originalGIOnChange(volume);
            SoundBoard.audioHelper.onVolumeChange(game.settings.get('SoundBoard', 'soundboardServerVolume') / 100);
        };
    }

    if (game.user.isGM) {
        SoundBoard.soundsError = false;
        await SoundBoard.getSounds();
        Handlebars.registerHelper(SoundBoard.handlebarsHelpers);
        Handlebars.registerPartial('SoundBoardPackageCard', await getTemplate('modules/SoundBoard/templates/partials/packagecard.hbs'));
    }

    SoundBoard.socketHelper = new SBSocketHelper();
    SoundBoard.audioHelper = new SBAudioHelper();

    if (game.settings.get('SoundBoard', 'forcePopoutCompat')) {

        // Very dirty - force our code over popout modules, allowing SB to play

        // Consider attaching the SoundBoard to the application somehow, instead of calling its singleton
        PopoutModule.prototype.createDocument = () => {
            const html = document.createElement('html');
            const head = document.importNode(document.getElementsByTagName('head')[0], true);
            const body = document.importNode(document.getElementsByTagName('body')[0], false);

            // for (const child of [...head.children]) {
            //     if (child.nodeName === "SCRIPT" && child.src) {
            //         const src = child.src.replace(window.location.origin, "");
            //         if (!src.match(/tinymce|jquery|webfont|pdfjs|SoundBoard/)) {
            //             //child.remove();
            //         }
            //     }
            // }
            html.appendChild(head);
            html.appendChild(body);
            return html;
        };
    }

});

Hooks.on('closeSoundBoardApplication', () => {
    if (SoundBoard.openedBoard?.rendered) {
        SoundBoard.openedBoard.close();
    }
});
Hooks.on('getSceneControlButtons', SoundBoard.addSoundBoard);
Hooks.on('renderSidebarTab', SoundBoard.addCustomPlaylistElements);

Hooks.once('WhetstoneReady', () => {

    // register a default menu with Whetstone for configuration
    game.Whetstone.settings.registerMenu('SoundBoardDefault', 'SoundBoardDefault', {
        name: game.i18n.localize('WHETSTONE.ConfigureTheme'),
        label: game.i18n.localize('WHETSTONE.ConfigureTheme'),
        hint: game.i18n.localize('WHETSTONE.ConfigHint'),
        icon: 'fas fa-paint-brush',
        restricted: false
    });


    let wsVars = [{
        name: '--SoundBoard-loop-bg-from',
        default: '#01701c',
        type: 'color',
        title: 'loopbgfrom',
        hint: '',
        presets: 'palette'
    }, {
        name: '--SoundBoard-loop-bg-to',
        default: '#1b291e',
        type: 'color',
        title: 'loopbgto',
        hint: 'loopbgto',
        presets: 'palette'
    }, {
        name: '--SoundBoard-loop-animation-time',
        default: '1s',
        type: String,
        title: 'loopanimationtime',
        hint: 'loopanimationtime'
    }, {
        name: '--SoundBoard-loop-active-icon-color',
        default: '#42e600',
        type: 'color',
        title: 'loopactiveiconcolor',
        hint: 'loopactiveiconcolor',
        presets: 'palette'
    }, {
        name: '--SoundBoard-loop-text-color',
        default: '#ffffff',
        type: 'color',
        title: 'looptextcolor',
        hint: 'looptextcolor',
        presets: 'palette'
    }, {
        name: '--SoundBoard-favorite-color',
        default: '#feeb20',
        type: 'color',
        title: 'favoritecolor',
        hint: 'favoritecolor',
        presets: 'palette'
    }, {
        name: '--SoundBoard-text-shadow', default: '0 0 2px #000', type: String, title: 'textshadow', hint: 'textshadow'
    }, {
        name: '--SoundBoard-more-button-active-color',
        default: '#969696',
        type: 'color',
        title: 'morebuttonactive',
        hint: 'morebuttonactive',
        presets: 'palette'
    }, {
        name: '--SoundBoard-more-button-inactive-color',
        default: 'rgba(0, 0, 0, 04)',
        type: String,
        title: 'morebuttoninactive',
        hint: 'morebuttoninactive'
    }, {
        name: '--SoundBoard-more-button-hover-color',
        default: '#dddddd',
        type: 'color',
        title: 'morebuttonhover',
        hint: 'morebuttonhover',
        presets: 'palette'
    }, {
        name: '--SoundBoard-extended-options-bg',
        default: 'rgba(100, 100, 120, 0.94)',
        type: String,
        title: 'extendedoptionsbg',
        hint: 'extendedoptionsbg'
    }, {
        name: '--SoundBoard-extended-options-icon-color',
        default: '#ffffff',
        type: 'color',
        title: 'extendedoptionsicon',
        hint: 'extendedoptionsicon',
        presets: 'palette'
    }, {
        name: '--SoundBoard-btn-text-size', default: '1rem', type: String, title: 'btntextsize', hint: ''
    }, {
        name: '--SoundBoard-toolbar-color',
        default: '#444444',
        type: 'color',
        title: 'toolbarcolor',
        hint: 'toolbarcolor',
        presets: 'palette'
    }, {
        name: '--SoundBoard-toolbar-btn-active-color',
        default: '#01701c',
        type: 'color',
        title: 'toolbarbtnactivecolor',
        hint: 'toolbarbtnactivecolor',
        presets: 'palette'
    }];

    wsVars = wsVars.map((el) => {
        if (el.title.length > 0) {
            el.title = `SOUNDBOARD.whetstone.var.${el.title}.title`;
        }
        if (el.hint.length > 0) {
            el.hint = `SOUNDBOARD.whetstone.var.${el.hint}.hint`;
        }
        return el;
    });

    // register a theme
    game.Whetstone.themes.register('SoundBoard', {
        id: 'SoundBoardDefault',
        name: 'SoundBoardDefault',
        title: game.i18n.localize('SOUNDBOARD.whetstone.theme.default.title'),
        description: game.i18n.localize('SOUNDBOARD.whetstone.theme.default.description'),
        version: '1.0.0',
        authors: [{
            name: 'Blitzkraig',
            contact: 'https://github.com/BlitzKraig/fvtt-SoundBoard',
            url: 'https://github.com/BlitzKraig/fvtt-SoundBoard'
        }],
        variables: wsVars,
        presets: {
            palette: {
                '#42e600': 'Vibrant Green',
                '#feeb20': 'Vibrant Yellow',
                '#01701c': 'Muted Green',
                '#1b291e': 'Darkened Green',
                '#ffffff': 'Pure White',
                '#dddddd': 'Lightest Grey',
                '#969696': 'Middlest Grey',
                '#444444': 'Darkest Grey'
            }
        },
        img: 'modules/SoundBoard/bundledDocs/default-theme-icon.png',
        preview: 'modules/SoundBoard/bundledDocs/default-theme-preview.png',
        systems: {
            'core': '0.6.6'
        }
    });
});